package com.example.rentit.sales.rest;

import com.example.rentit.RentItApplication;
import com.example.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.rentit.sales.application.dto.PurchaseOrderDTO;
import com.example.rentit.sales.domain.model.POStatus;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PurchaseOrderRestControllerTests {

    private MockMvc mockMvc;

    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    // PS1
    @Test
    public void testGetAllPurchaseOrders() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode orders = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        assertThat(orders.size()).isEqualTo(8);
    }

    // PS2
    @Test
    public void testGetPurchaseOrderSuccessful() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo("1");
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("ACCEPTED");
    }

    // PS3
    @Test
    public void testGetPurchaseOrderFailure() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/99999")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
        String errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("Error: could not find purchase order");
    }

    // PS4
    @Test
    public void testCreatePurchaseOrderSuccess() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/entries/available?name=exc&startDate=2020-08-14&endDate=2020-08-25")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantsJSON = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("plantInventoryEntryDToes");
        List<PlantInventoryEntryDTO> plants = mapper.readValue(plantsJSON.toString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        assertThat(plants.size()).isEqualTo(3);

        CreationBody order = new CreationBody();
        order.setPlantId(1L);
        order.setStartDate(LocalDate.of(2020, 9, 14));
        order.setEndDate(LocalDate.of(2020, 9, 25));

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    // PS4
    @Test
    public void testCreatePurchaseOrderFailure() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(1L);
        order.setStartDate(LocalDate.of(2010, 9, 14));
        order.setEndDate(LocalDate.of(2010, 9, 25));

        MvcResult result = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
        String errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("Invalid Interval");
    }

    // PS5
    @Test
    public void testGetPurchaseOrderAndCheckStatus() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo("1");
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("ACCEPTED");
    }

    // PS8
    @Test
    public void testPurchaseOrderDispatching() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(3L);
        order.setStartDate(LocalDate.of(2020, 6, 1));
        order.setEndDate(LocalDate.of(2020, 6, 4));
        mockMvc.perform(post("/api/sales/orders/")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        MvcResult getId = mockMvc.perform(get("/api/sales/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(getId.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        List<PurchaseOrderDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        long id = plantHireRequests.get(plantHireRequests.size() - 1).get_id();

        // Make status ACCEPTED for happy path
        mockMvc.perform(post("/api/sales/orders/" + id + "/accept")).andReturn();

        // make dispatched
        MvcResult result = mockMvc.perform(post("/api/sales/orders/" + id + "/dispatch")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        PurchaseOrderDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.PLANT_DISPATCHED, returnedOrder.getStatus());
    }

    // PS8
    @Test
    public void testPurchaseOrderDispatchingFailsWhenRejected() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(3L);
        order.setStartDate(LocalDate.of(2020, 6, 1));
        order.setEndDate(LocalDate.of(2020, 6, 4));
        mockMvc.perform(post("/api/sales/orders/")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        MvcResult getId = mockMvc.perform(get("/api/sales/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(getId.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        List<PurchaseOrderDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        long id = plantHireRequests.get(plantHireRequests.size() - 1).get_id();

        // Make status ACCEPTED for happy path
        mockMvc.perform(post("/api/sales/orders/" + id + "/reject")).andReturn();

        // make dispatched (should fail)
        MvcResult result = mockMvc.perform(post("/api/sales/orders/" + id + "/dispatch")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        PurchaseOrderDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.REJECTED, returnedOrder.getStatus());
    }

    // PS7
    @Test
    public void testPurchaseOrderCancellation() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(3L);
        order.setStartDate(LocalDate.of(2020, 6, 1));
        order.setEndDate(LocalDate.of(2020, 6, 4));
        mockMvc.perform(post("/api/sales/orders/")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        MvcResult getId = mockMvc.perform(get("/api/sales/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(getId.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        List<PurchaseOrderDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        long id = plantHireRequests.get(plantHireRequests.size() - 1).get_id();
        MvcResult result = mockMvc.perform(post("/api/sales/orders/" + id + "/cancel")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        PurchaseOrderDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.CANCELLED, returnedOrder.getStatus());
    }

    // PS7
    @Test
    public void testPurchaseOrderCancellationAfterDispatched() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(3L);
        order.setStartDate(LocalDate.of(2020, 6, 1));
        order.setEndDate(LocalDate.of(2020, 6, 4));
        mockMvc.perform(post("/api/sales/orders/")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        MvcResult getId = mockMvc.perform(get("/api/sales/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(getId.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        List<PurchaseOrderDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        long id = plantHireRequests.get(plantHireRequests.size() - 1).get_id();

        // Make status DISPATCHED for sad path to see if cancellation fails (since we cant cancel it anymore, already dispatched)
        mockMvc.perform(post("/api/sales/orders/" + id + "/accept")).andReturn();
        mockMvc.perform(post("/api/sales/orders/" + id + "/dispatch")).andReturn();

        MvcResult result = mockMvc.perform(post("/api/sales/orders/" + id + "/cancel")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        PurchaseOrderDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.PLANT_DISPATCHED, returnedOrder.getStatus());
    }

    @Test
    public void testPurchaseOrderAcceptance() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(3L);
        order.setStartDate(LocalDate.of(2020, 6, 1));
        order.setEndDate(LocalDate.of(2020, 6, 4));
        mockMvc.perform(post("/api/sales/orders/")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        MvcResult getId = mockMvc.perform(get("/api/sales/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(getId.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        List<PurchaseOrderDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        long id = plantHireRequests.get(plantHireRequests.size() - 1).get_id();
        MvcResult result = mockMvc.perform(post("/api/sales/orders/" + id + "/accept")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        PurchaseOrderDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.ACCEPTED, returnedOrder.getStatus());
    }

    @Test
    public void testPurchaseOrderRejection() throws Exception {
        CreationBody order = new CreationBody();
        order.setPlantId(3L);
        order.setStartDate(LocalDate.of(2020, 6, 1));
        order.setEndDate(LocalDate.of(2020, 6, 4));
        mockMvc.perform(post("/api/sales/orders/")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        MvcResult getId = mockMvc.perform(get("/api/sales/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(getId.getResponse().getContentAsString())
                .path("_embedded")
                .path("purchaseOrderDToes");

        List<PurchaseOrderDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        long id = plantHireRequests.get(plantHireRequests.size() - 1).get_id();
        MvcResult result = mockMvc.perform(post("/api/sales/orders/" + id + "/reject")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        PurchaseOrderDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.REJECTED, returnedOrder.getStatus());
    }

    // PS9
    @Test
    public void testPurchaseOrderMarkAsDeliveredSuccess() throws Exception {
        long id = 7;
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PLANT_DISPATCHED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/mark-as-delivered")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();


        PurchaseOrderDTO returnedOrder = mapper.readValue(result2.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.PLANT_DELIVERED, returnedOrder.getStatus());
    }

    // PS9
    @Test
    public void testPurchaseOrderMarkAsDeliveredFail() throws Exception {
        long id = 1;
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("ACCEPTED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/mark-as-delivered")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();


        String errorMessage = result2.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("PO cannot be marked as delivered, as it is not dispatched");
    }

    // PS9
    @Test
    public void testPurchaseOrderRejectByCustomerSuccess() throws Exception {
        long id = 7;
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PLANT_DISPATCHED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/reject-by-customer")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();


        PurchaseOrderDTO returnedOrder = mapper.readValue(result2.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.REJECTED_BY_CUSTOMER, returnedOrder.getStatus());
    }

    // PS9
    @Test
    public void testPurchaseOrderRejectByCustomerFail() throws Exception {
        long id = 4;
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("ACCEPTED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/reject-by-customer")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        String errorMessage = result2.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("PO cannot be rejected by customer if it has not been dispatched");
    }

    // PS10
    @Test
    public void testPurchaseOrderReturnSuccess() throws Exception {
        long id = 6;
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PLANT_DELIVERED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/return")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();


        PurchaseOrderDTO returnedOrder = mapper.readValue(result2.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.PLANT_RETURNED, returnedOrder.getStatus());
    }

    // PS10
    @Test
    public void testPurchaseOrderReturnFail() throws Exception {
        long id = 4;
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("ACCEPTED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/return")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        String errorMessage = result2.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("PO cannot be returned, as it is not delivered");
    }

    //PS13
    @Test
    public void testPurchaseOrderReturnSuccessCreatesInvoice() throws Exception {
        MvcResult result0 = mockMvc.perform(get("/api/invoices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode invoices1 = mapper.readTree(result0.getResponse().getContentAsString())
                .path("_embedded")
                .path("invoiceDToes");

        assertThat(invoices1.size()).isEqualTo(0);

        long id = 6;
        MvcResult result1 = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result1.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PLANT_DELIVERED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/return")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();


        PurchaseOrderDTO returnedOrder = mapper.readValue(result2.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertEquals(POStatus.PLANT_RETURNED, returnedOrder.getStatus());

        MvcResult result3 = mockMvc.perform(get("/api/invoices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode invoices2 = mapper.readTree(result3.getResponse().getContentAsString())
                .path("_embedded")
                .path("invoiceDToes");

        assertThat(invoices2.size()).isEqualTo(1);
    }

    //PS13
    @Test
    public void testPurchaseOrderReturnFailDoesNotCreateInvoice() throws Exception {
        MvcResult result0 = mockMvc.perform(get("/api/invoices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode invoices1 = mapper.readTree(result0.getResponse().getContentAsString())
                .path("_embedded")
                .path("invoiceDToes");

        assertThat(invoices1.size()).isEqualTo(0);

        long id = 4;
        MvcResult result1 = mockMvc.perform(get("/api/sales/orders/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result1.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo(String.valueOf(id));
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("ACCEPTED");

        MvcResult result2 = mockMvc.perform(post("/api/sales/orders/" + id + "/return")
                .content(mapper.writeValueAsString(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        String errorMessage = result2.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("PO cannot be returned, as it is not delivered");

        MvcResult result3 = mockMvc.perform(get("/api/invoices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode invoices2 = mapper.readTree(result3.getResponse().getContentAsString())
                .path("_embedded")
                .path("invoiceDToes");

        assertThat(invoices2.size()).isEqualTo(0);
    }

    @Test
    public void testPurchaseOrderExtensionSuccess() throws Exception {
        MvcResult result = mockMvc.perform(patch("/api/sales/orders/1/extend?to=2020-07-20")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(String.valueOf(true));
    }

    @Test
    public void testPurchaseOrderExtensionFail() throws Exception {
        MvcResult result = mockMvc.perform(patch("/api/sales/orders/1/extend?to=2005-06-09")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    //PS6
    @Test
    public void testPurchaseOrderModifySuccess() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo("1");
        assertThat(tree.at("/rentalPeriod").toString().replaceAll("^\"|\"$", "")).isEqualTo("{\"startDate\":\"2018-03-22\",\"endDate\":\"2018-03-24\"}");

        mockMvc.perform(patch("/api/sales/orders/1/modify?from=2022-06-09&to=2022-07-09")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult result2 = mockMvc.perform(get("/api/sales/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order2 = mapper.readTree(result2.getResponse().getContentAsString());
        JsonParser parser2 = new JsonFactory().createParser(order2.toString());
        parser2.setCodec(new ObjectMapper());
        TreeNode tree2 = parser2.readValueAsTree();
        assertThat(tree2.at("/_id").toString()).isEqualTo("1");
        assertThat(tree2.at("/rentalPeriod").toString().replaceAll("^\"|\"$", "")).isEqualTo("{\"startDate\":\"2022-06-09\",\"endDate\":\"2022-07-09\"}");
    }

    //PS6
    @Test
    public void testPurchaseOrderModifyFail() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo("1");
        assertThat(tree.at("/rentalPeriod").toString().replaceAll("^\"|\"$", "")).isEqualTo("{\"startDate\":\"2018-03-22\",\"endDate\":\"2018-03-24\"}");

        mockMvc.perform(patch("/api/sales/orders/1/modify?from=2005-06-09&to=2010-06-09")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        MvcResult result2 = mockMvc.perform(get("/api/sales/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order2 = mapper.readTree(result2.getResponse().getContentAsString());
        JsonParser parser2 = new JsonFactory().createParser(order2.toString());
        parser2.setCodec(new ObjectMapper());
        TreeNode tree2 = parser2.readValueAsTree();
        assertThat(tree2.at("/_id").toString()).isEqualTo("1");
        assertThat(tree2.at("/rentalPeriod").toString().replaceAll("^\"|\"$", "")).isEqualTo("{\"startDate\":\"2018-03-22\",\"endDate\":\"2018-03-24\"}");
    }
}
