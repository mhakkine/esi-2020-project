package com.example.rentit.maintenance;

import com.example.rentit.RentItApplication;
import com.example.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.rentit.maintenance.domain.model.MaintenanceOrder;
import com.example.rentit.maintenance.domain.repository.MaintenanceOrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

@ContextConfiguration(classes = RentItApplication.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class CancelCompletedOrderTest {

    private MockMvc mockMvc;
    private List<PlantInventoryEntryDTO> availablePlants;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    MaintenanceOrderRepository maintenanceOrderRepository;

    @Before  // Use `Before` from Cucumber library
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After  // Use `After` from Cucumber library
    public void tearOff() {
        plantInventoryItemRepository.deleteAll();
        plantInventoryEntryRepository.deleteAll();
    }

    @Given("^the following inventory entry catalog_1$")
    public void the_following_plant_entry_catalog(List<PlantInventoryEntry> entries) throws Throwable {
        plantInventoryEntryRepository.saveAll(entries);
    }

    @Given("^the following inventory item catalog_1$")
    public void the_following_plant_item_catalog(List<PlantInventoryItem> entries) throws Throwable {
        plantInventoryItemRepository.saveAll(entries);
    }

    @Given("^the following maintenance order catalog_1$")
    public void the_following_maintenance_order_catalog(List<MaintenanceOrder> entries) throws Throwable {
        maintenanceOrderRepository.saveAll(entries);
    }
    // did not finish, because cucumber is not very good at reading LocalDates from tables, therefore was stuck at this and could not do actual coding
}
