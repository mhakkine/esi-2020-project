package com.example.rentit.maintenance.rest;

import com.example.rentit.RentItApplication;
import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.domain.model.EquipmentCondition;
import com.example.rentit.maintenance.application.dto.MaintenanceTaskCreationBody;
import com.example.rentit.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.rentit.maintenance.domain.model.MaintenanceTask;
import com.example.rentit.maintenance.domain.model.TypeOfWork;
import com.example.rentit.maintenance.domain.repository.MaintenanceTaskRepository;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaintenanceTaskRestControllerTests {

    @Mock
    MaintenanceTaskRepository repo;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        initMocks(this);
    }

    @Test
    public void testGetAllMaintenanceTasks() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks.size()).isEqualTo(2);
    }

    @Test
    public void testGetMaintenanceTaskSuccessful() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/maintenance/tasks/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree = parser.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo("1");
        assertThat(tree.at("/type").toString().replaceAll("^\"|\"$", "")).isEqualTo("PREVENTIVE");
    }

    @Test
    public void testGetMaintenanceTaskFail() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/maintenance/tasks/99999")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
        String errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("Error: could not find maintenance task with id");
    }

    // PS11
    @Test
    public void testCreateMaintenanceTaskSuccess() throws Exception {
        MvcResult result1 = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks1 = mapper.readTree(result1.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks1.size()).isEqualTo(2);

        MaintenanceTaskCreationBody body = new MaintenanceTaskCreationBody();
        body.setType(TypeOfWork.PREVENTIVE);
        body.setReservationId(1l);
        body.setDescription("test desc");

        body.setStartDate(LocalDate.of(2021, 4, 14));
        body.setEndDate(LocalDate.of(2021, 4, 25));

        mockMvc.perform(post("/api/maintenance/tasks").content(mapper.writeValueAsString(body)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        MvcResult result2 = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks2 = mapper.readTree(result2.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks2.size()).isEqualTo(3);
    }

    //PS11
    @Test
    public void testCreateMaintenanceTaskFail() throws Exception {
        MaintenanceTaskCreationBody body = new MaintenanceTaskCreationBody();
        body.setType(TypeOfWork.PREVENTIVE);
        body.setReservationId(1l);
        body.setDescription("test desc");
        body.setStartDate(LocalDate.of(2055, 4, 14));
        body.setEndDate(LocalDate.of(2021, 4, 25));

        MvcResult result = mockMvc.perform(post("/api/maintenance/tasks")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();

        String errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).isEqualTo("Invalid start/end dates");
    }

    // PS12
    @Test
    public void testCreateMaintenanceTaskCancelsPOs() throws Exception {
        MvcResult result0 = mockMvc.perform(get("/api/sales/orders/8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order1 = mapper.readTree(result0.getResponse().getContentAsString());
        JsonParser parser1 = new JsonFactory().createParser(order1.toString());
        parser1.setCodec(new ObjectMapper());
        TreeNode tree = parser1.readValueAsTree();
        assertThat(tree.at("/_id").toString()).isEqualTo("8");
        assertThat(tree.at("/plantId").toString()).isEqualTo("1");
        assertThat(tree.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PENDING");

        MvcResult result1 = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks1 = mapper.readTree(result1.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks1.size()).isEqualTo(2);

        MaintenanceTaskCreationBody body = new MaintenanceTaskCreationBody();
        body.setType(TypeOfWork.PREVENTIVE);
        body.setReservationId(1l);
        body.setDescription("test desc");

        body.setStartDate(LocalDate.of(2020, 6, 14));
        body.setEndDate(LocalDate.of(2026, 4, 25));

        mockMvc.perform(post("/api/maintenance/tasks").content(mapper.writeValueAsString(body)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        MvcResult result2 = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks2 = mapper.readTree(result2.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks2.size()).isEqualTo(3);

        MvcResult result = mockMvc.perform(get("/api/sales/orders/8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order = mapper.readTree(result.getResponse().getContentAsString());
        JsonParser parser = new JsonFactory().createParser(order.toString());
        parser.setCodec(new ObjectMapper());
        TreeNode tree2 = parser.readValueAsTree();
        assertThat(tree2.at("/_id").toString()).isEqualTo("8");
        assertThat(tree2.at("/plantId").toString()).isEqualTo("1");
        assertThat(tree2.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("CANCELLED");
    }

    // PS12
    @Test
    public void testCreateMaintenanceTaskDoesNotCancelPOs() throws Exception {
        MvcResult result1 = mockMvc.perform(get("/api/sales/orders/8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order1 = mapper.readTree(result1.getResponse().getContentAsString());
        JsonParser parser1 = new JsonFactory().createParser(order1.toString());
        parser1.setCodec(new ObjectMapper());
        TreeNode tree1 = parser1.readValueAsTree();
        assertThat(tree1.at("/_id").toString()).isEqualTo("8");
        assertThat(tree1.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PENDING");

        MvcResult result2 = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks1 = mapper.readTree(result2.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks1.size()).isEqualTo(2);

        MaintenanceTaskCreationBody body = new MaintenanceTaskCreationBody();
        body.setType(TypeOfWork.PREVENTIVE);
        body.setReservationId(1l);
        body.setDescription("test desc");

        body.setStartDate(LocalDate.of(2021, 8, 14));
        body.setEndDate(LocalDate.of(2021, 8, 25));

        mockMvc.perform(post("/api/maintenance/tasks").content(mapper.writeValueAsString(body)).contentType(MediaType.APPLICATION_JSON));

        MvcResult result3 = mockMvc.perform(get("/api/maintenance/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode maintenanceTasks2 = mapper.readTree(result3.getResponse().getContentAsString())
                .path("_embedded")
                .path("maintenanceTaskDToes");

        assertThat(maintenanceTasks2.size()).isEqualTo(3);

        MvcResult result4 = mockMvc.perform(get("/api/sales/orders/8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        JsonNode order2 = mapper.readTree(result4.getResponse().getContentAsString());
        JsonParser parser2 = new JsonFactory().createParser(order2.toString());
        parser2.setCodec(new ObjectMapper());
        TreeNode tree2 = parser2.readValueAsTree();
        assertThat(tree2.at("/_id").toString()).isEqualTo("8");
        assertThat(tree2.at("/status").toString().replaceAll("^\"|\"$", "")).isEqualTo("PENDING");
    }

    @Test
    public void testUpdateTask() throws Exception {
        MaintenanceTaskDTO maintenanceTaskDTO = new MaintenanceTaskDTO();
        maintenanceTaskDTO.set_id(1l);
        maintenanceTaskDTO.setSerialNumber("23");
        maintenanceTaskDTO.setType(TypeOfWork.PREVENTIVE);
        maintenanceTaskDTO.setReservationId(1l);
        maintenanceTaskDTO.setDescription("test desc");
        maintenanceTaskDTO.setEquipmentCondition(EquipmentCondition.SERVICEABLE);

        BusinessPeriodDTO of = BusinessPeriodDTO.of(LocalDate.of(2021, 4, 14), LocalDate.of(2021, 4, 25));
        maintenanceTaskDTO.setMaintenancePeriod(of);

        when(repo.save(any())).thenReturn(new MaintenanceTask());

        mockMvc.perform(put("/api/maintenance/tasks/1").content(mapper.writeValueAsString(maintenanceTaskDTO)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
