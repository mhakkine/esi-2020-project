package com.example.rentit.maintenance;

import com.example.rentit.RentItApplication;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.rentit.maintenance.domain.model.MaintenanceOrder;
import com.example.rentit.maintenance.domain.repository.MaintenanceOrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = RentItApplication.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class CreateMaintenanceRequestTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    MaintenanceOrderRepository maintenanceOrderRepository;

    @Before  // Use `Before` from Cucumber library
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After  // Use `After` from Cucumber library
    public void tearOff() {
        plantInventoryItemRepository.deleteAll();
        plantInventoryEntryRepository.deleteAll();
    }

    @Given("^the following inventory entry catalog$")
    public void the_following_plant_entry_catalog(List<PlantInventoryEntry> entries) throws Throwable {
        plantInventoryEntryRepository.saveAll(entries);
    }

    @Given("^the following inventory item catalog$")
    public void the_following_plant_item_catalog(List<PlantInventoryItem> entries) throws Throwable {
        plantInventoryItemRepository.saveAll(entries);
    }

    @Given("^the following maintenance order catalog$")
    public void the_following_maintenance_order_catalog(List<MaintenanceOrder> entries) throws Throwable {
        maintenanceOrderRepository.saveAll(entries);
    }

    @When("^the customer adds a new maintenance order with the following data: start_date: \"([^\"]*)\", end_date: \"([^\"]*)\", plant_id: \"([^\"]*)\", site: \"([^\"]*)\", description: \"([^\"]*)\", engineer_name: \"([^\"]*)\"$")
    public void the_customer_queries_the_plant_catalog_for_an_available_from_to(LocalDate start, String end, long plant, String site, String description, String engineer) throws Throwable {
        String url = String.format("/api/maintenance/order");
        String content = String.format("{\"start\":\"%s\",", start) + String.format("{\"end\":\"%s\",", end) + String.format("{\"plantId\":\"%s\",", plant) + String.format("{\"description\":\"%s\",", description) + String.format("{\"site\":\"%s\",", site) + String.format("{\"engineerName\":\"%s\",", engineer);

        MvcResult result = mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk())
                .andReturn();
        // did not finish, because cucumber is not very good at reading LocalDates from tables, therefore was stuck at this and could not do actual coding
    }
}


