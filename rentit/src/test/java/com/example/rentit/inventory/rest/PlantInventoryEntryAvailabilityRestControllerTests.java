package com.example.rentit.inventory.rest;

import com.example.rentit.RentItApplication;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class) // Check if the name of this class is correct or not
@ActiveProfiles("test")
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantInventoryEntryAvailabilityRestControllerTests {

    private MockMvc mockMvc;

    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    // PS3
    @Test
    public void testGetAvailabilityOfAvailable() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/entries/1/availability?startDate=2020-03-24&endDate=2021-01-01")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String result2 = result.getResponse().getContentAsString();
        assertThat(result2).isEqualTo("true");
    }

    // PS3
    @Test
    public void testGetAvailabilityOfNotAvailable() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/entries/13/availability?startDate=2020-03-24&endDate=2021-05-01")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String result2 = result.getResponse().getContentAsString();
        assertThat(result2).isEqualTo("false");
    }
}
