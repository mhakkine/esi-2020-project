package com.example.rentit.inventory.repository;

import com.example.rentit.RentItApplication;
import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.model.PlantReservation;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository2;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.rentit.inventory.domain.repository.PlantReservationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class InventoryRepositoryTest {

    @Autowired
    PlantInventoryEntryRepository2 plantInventoryEntryRepo2;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;

    @Autowired
    PlantReservationRepository plantReservationRepo;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Test
    public void queryPlantCatalog() {
        assertThat(plantInventoryEntryRepo2.count()).isEqualTo(14l);
    }

    @Test
    public void queryByName() {
        assertThat(plantInventoryEntryRepo2.findByNameContaining("Mini").size()).isEqualTo(2);

        PlantInventoryEntry p1 = plantInventoryEntryRepo2.findById(1L).orElse(null);
        PlantInventoryEntry p2 = plantInventoryEntryRepo2.findById(2L).orElse(null);
        assertThat(p1).isNotNull();
        assertThat(p1).isNotNull();

        List<PlantInventoryEntry> res1 = plantInventoryEntryRepo2.finderMethod("Mini");
        List<PlantInventoryEntry> res2 = plantInventoryEntryRepo2.finderMethodV2("Mini");

        assertThat(res1).containsExactly(p1, p2);
        assertThat(res2).containsExactly(p1, p2);
    }

    @Test
    public void findAvailableTest() {
        PlantInventoryEntry entry = plantInventoryEntryRepo2.findById(1l).orElse(null);
        PlantInventoryItem item = plantInventoryItemRepo.findOneByPlantInfo(entry);

        assertThat(plantInventoryEntryRepository.findAvailablePlants("Mini", LocalDate.of(2020, 2, 20), LocalDate.of(2020, 2, 25)))
                .contains(entry);

        PlantReservation po = PlantReservation.of(
                item,
                BusinessPeriod.of(LocalDate.of(2020, 2, 20), LocalDate.of(2020, 2, 25))
        );
        plantReservationRepo.save(po);

        assertThat(plantInventoryEntryRepository.findAvailablePlants("Mini", LocalDate.of(2020, 2, 20), LocalDate.of(2020, 2, 25)))
                .doesNotContain(entry);
    }
}
