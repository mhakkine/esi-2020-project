package com.example.rentit.inventory.domain.repository;

import com.example.rentit.inventory.domain.model.PlantInventoryItem;

import java.time.LocalDate;
import java.util.List;

public interface CustomPlantInventoryItemRepository {
    boolean getItemAvailabilityById(Long id, LocalDate startDate, LocalDate endDate);

    List<PlantInventoryItem> findAvailableItems(String name, LocalDate startDate, LocalDate endDate);

    List<PlantInventoryItem> findAvailableItemsSkipExisting(List<Long> reservationIDs, String name, LocalDate startDate, LocalDate endDate);
}
