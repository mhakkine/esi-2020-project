package com.example.rentit.inventory.application.service;

import com.example.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.repository.CustomPlantInventoryItemRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PlantInventoryItemService {

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    CustomPlantInventoryItemRepository customPlantInventoryItemRepository;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public CollectionModel<PlantInventoryItemDTO> all() {
        return plantInventoryItemAssembler.toCollectionModel(plantInventoryItemRepository.findAll());
    }

    public PlantInventoryItem findItemById(Long id) {
        return plantInventoryItemRepository.findById(id).orElse(null);
    }

    public CollectionModel<PlantInventoryItemDTO> findAvailableItems(String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryItem> items = customPlantInventoryItemRepository.findAvailableItems(name, startDate, endDate);
        return plantInventoryItemAssembler.toCollectionModel(items);
    }

    public CollectionModel<PlantInventoryItemDTO> findAvailableItemsSkipExisting(List<Long> reservationIDs, String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryItem> items = customPlantInventoryItemRepository.findAvailableItemsSkipExisting(reservationIDs, name, startDate, endDate);
        return plantInventoryItemAssembler.toCollectionModel(items);
    }

    public List<PlantInventoryItem> findAvailableItemsE(String name, LocalDate startDate, LocalDate endDate) {
        return customPlantInventoryItemRepository.findAvailableItems(name, startDate, endDate);
    }
}
