package com.example.rentit.inventory.rest;

import com.example.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.rentit.inventory.application.service.PlantInventoryEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/inventory/entries")
public class PlantInventoryEntryRestController {
    @Autowired
    PlantInventoryEntryService plantInventoryEntryService;

    @GetMapping("/{id]")
    @ResponseStatus(HttpStatus.OK)
    public PlantInventoryEntryDTO fetchPlant(@PathVariable Long id) {
        PlantInventoryEntryDTO plantInventoryEntryDto = plantInventoryEntryService.findEntryDTOById(id);
        return plantInventoryEntryDto;
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public CollectionModel<PlantInventoryEntryDTO> fetchPlants() {
        try {
            return plantInventoryEntryService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get plant inventory entries", e);
        }
    }

    @GetMapping("/available")
    public CollectionModel<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        try {
            return plantInventoryEntryService.findAvailablePlants(plantName, startDate, endDate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get plant inventory entries", e);
        }
    }
}
