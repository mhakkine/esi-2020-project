package com.example.rentit.inventory.domain.repository;

import com.example.rentit.inventory.domain.model.PlantReservation;

import java.util.List;

public interface CustomPlantReservationRepository {
    public PlantReservation getPlantReservationByPurchaseOrderId(Long id);

    public List<PlantReservation> getPlantReservationsByPurchaseOrderId(Long id);
}
