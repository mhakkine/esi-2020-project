package com.example.rentit.inventory.domain.model;

import com.example.rentit.sales.domain.model.PurchaseOrder;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PlantReservation {
    @Id
    @GeneratedValue
    Long id;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PurchaseOrder rental;

    @ManyToOne
    PlantInventoryItem plant;

    public static PlantReservation of(PlantInventoryItem item, BusinessPeriod schedule) {
        PlantReservation reservation = new PlantReservation();
        reservation.plant = item;
        reservation.schedule = schedule;
        return reservation;
    }
}
