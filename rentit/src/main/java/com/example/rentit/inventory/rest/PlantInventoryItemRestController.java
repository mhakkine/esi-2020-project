package com.example.rentit.inventory.rest;

import com.example.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.example.rentit.inventory.application.service.PlantInventoryItemService;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/inventory/items")
public class PlantInventoryItemRestController {
    @Autowired
    PlantInventoryItemService plantInventoryItemService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public CollectionModel<PlantInventoryItemDTO> findAllPlants() {
        try {
            return plantInventoryItemService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get plant inventory entries", e);
        }
    }

    @GetMapping("/available")
    public List<PlantInventoryItem> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        try {
            return plantInventoryItemService.findAvailableItemsE(plantName, startDate, endDate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get plant inventory entries", e);
        }
    }

}
