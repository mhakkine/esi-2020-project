package com.example.rentit.inventory.domain.repository;

import com.example.rentit.inventory.domain.model.PlantReservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class CustomPlantReservationRepositoryImpl implements CustomPlantReservationRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<PlantReservation> getPlantReservationsByPurchaseOrderId(Long id) {
        return em.createQuery("select pr from PlantReservation pr where pr.rental.id = ?1",
                PlantReservation.class)
                .setParameter(1, id)
                .getResultList();
    }

    @Override
    public PlantReservation getPlantReservationByPurchaseOrderId(Long id) {
        return em.createQuery("select pr from PlantReservation pr where pr.rental.id = ?1",
                PlantReservation.class)
                .setParameter(1, id)
                .getSingleResult();
    }
}
