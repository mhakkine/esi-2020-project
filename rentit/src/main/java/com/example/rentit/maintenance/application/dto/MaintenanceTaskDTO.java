package com.example.rentit.maintenance.application.dto;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.domain.model.EquipmentCondition;
import com.example.rentit.maintenance.domain.model.TypeOfWork;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class MaintenanceTaskDTO extends RepresentationModel<MaintenanceTaskDTO> {

    private Long _id;

    private BusinessPeriodDTO maintenancePeriod;

    private String description;

    private TypeOfWork type;

    private Long reservationId;

    private String serialNumber;

    private EquipmentCondition equipmentCondition;

}