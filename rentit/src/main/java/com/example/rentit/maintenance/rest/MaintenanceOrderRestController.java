package com.example.rentit.maintenance.rest;

import com.example.rentit.maintenance.application.dto.MaintenanceOrderDTO;
import com.example.rentit.maintenance.application.services.MaintenanceOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/maintenance/orders")
public class MaintenanceOrderRestController {

    @Autowired
    MaintenanceOrderService maintenanceOrderService;

    @GetMapping("")
    public CollectionModel<MaintenanceOrderDTO> fetchMaintenanceOrders() {
        try {
            return maintenanceOrderService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not find maintenance order with id", e);
        }
    }

    @GetMapping("/{id}")
    public MaintenanceOrderDTO fetchMaintenanceOrder(@PathVariable("id") Long id) {
        try {
            return maintenanceOrderService.findMO(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not find maintenance order with id", e);
        }
    }

    @PostMapping("")
    public ResponseEntity<MaintenanceOrderDTO> createMaintenanceOrder(@RequestBody MaintenanceOrderCreationBody requestBody) throws Exception {
        MaintenanceOrderDTO maintenanceOrder = maintenanceOrderService.createMO(requestBody.getStartDate(), requestBody.getEndDate(), requestBody.getPlantId(), requestBody.getDescription(), requestBody.getSite(), requestBody.getEngineerName());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(maintenanceOrder.getRequiredLink(IanaLinkRelations.SELF).toUri());

        return new ResponseEntity<>(maintenanceOrder, headers, HttpStatus.CREATED);
    }

    @PostMapping("/{id}/cancel")
    public MaintenanceOrderDTO cancelMaintenanceOrder(@PathVariable Long id) throws Exception {
        return maintenanceOrderService.cancelMO(id);
    }

    @PostMapping("/{id}/accept")
    public MaintenanceOrderDTO acceptMaintenanceOrder(@PathVariable Long id) throws Exception {
        return maintenanceOrderService.acceptMO(id);
    }

    @PostMapping("/{id}/reject")
    public MaintenanceOrderDTO rejectMaintenanceOrder(@PathVariable Long id) throws Exception {
        return maintenanceOrderService.rejectMO(id);
    }

    @PostMapping("/{id}/complete")
    public MaintenanceOrderDTO completeMaintenanceOrder(@PathVariable Long id) throws Exception {
        return maintenanceOrderService.completeMO(id);
    }

}
