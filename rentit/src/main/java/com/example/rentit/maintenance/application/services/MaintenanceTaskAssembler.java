package com.example.rentit.maintenance.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.service.PlantInventoryItemAssembler;
import com.example.rentit.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.rentit.maintenance.domain.model.MaintenanceTask;
import com.example.rentit.maintenance.rest.MaintenanceTaskRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class MaintenanceTaskAssembler extends RepresentationModelAssemblerSupport<MaintenanceTask, MaintenanceTaskDTO> {

    public MaintenanceTaskAssembler() {
        super(MaintenanceTaskRestController.class, MaintenanceTaskDTO.class);
    }

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Override
    public MaintenanceTaskDTO toModel(MaintenanceTask maintenanceTask) {
        MaintenanceTaskDTO dto = createModelWithId(maintenanceTask.getId(), maintenanceTask);
        dto.set_id(maintenanceTask.getId());
        dto.setDescription(maintenanceTask.getDescription());
        dto.setEquipmentCondition(maintenanceTask.getReservation().getEquipmentCondition());
        dto.setMaintenancePeriod(BusinessPeriodDTO.of(maintenanceTask.getMaintenancePeriod().getStartDate(), maintenanceTask.getMaintenancePeriod().getEndDate()));
        dto.setReservationId(maintenanceTask.getReservation().getId());
        dto.setSerialNumber(maintenanceTask.getReservation().getSerialNumber());
        dto.setType(maintenanceTask.getType_of_work());
        dto.add(linkTo(methodOn(MaintenanceTaskRestController.class).fetchMaintenanceTask(dto.get_id())).withRel("fetch"));

        return dto;
    }
}
