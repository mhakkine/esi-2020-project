package com.example.rentit.maintenance.rest;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class MaintenanceOrderCreationBody {

    private Long plantId;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;
    private String site;
    private String engineerName;

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getEngineerName() {
        return engineerName;
    }

    public void setEngineerName(String engineerName) {
        this.engineerName = engineerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "MaintenanceOrderCreationBody{" +
                "plantId=" + plantId +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", site='" + site + '\'' +
                ", engineerName='" + engineerName + '\'' +
                '}';
    }

}