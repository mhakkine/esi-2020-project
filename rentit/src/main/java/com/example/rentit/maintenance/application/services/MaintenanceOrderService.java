package com.example.rentit.maintenance.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodValidator;
import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.rentit.maintenance.application.dto.MaintenanceOrderDTO;
import com.example.rentit.maintenance.domain.model.MOStatus;
import com.example.rentit.maintenance.domain.model.MaintenanceOrder;
import com.example.rentit.maintenance.domain.repository.MaintenanceOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.time.LocalDate;
import java.util.List;

@Service
public class MaintenanceOrderService {

    @Autowired
    MaintenanceOrderRepository maintenanceOrderRepository;

    @Autowired
    PlantInventoryItemRepository inventoryItemRepository;

    @Autowired
    MaintenanceOrderAssembler maintenanceOrderAssembler;

    @Autowired
    BusinessPeriodValidator businessPeriodValidator;

    public CollectionModel<MaintenanceOrderDTO> all() {
        List<MaintenanceOrder> mo = maintenanceOrderRepository.findAll();
        return maintenanceOrderAssembler.toCollectionModel(mo);
    }

    public MaintenanceOrderDTO findMO(Long id) {
        MaintenanceOrder mo = maintenanceOrderRepository.findById(id).orElse(null);
        return maintenanceOrderAssembler.toModel(mo);
    }

    public MaintenanceOrderDTO createMO(LocalDate start, LocalDate end, Long plantId, String description, String site, String engineerName) throws Exception {
        if (start == null) {
            throw new Exception("start cannot be null");
        }

        if (end == null) {
            throw new Exception("end cannot be null");
        }

        BusinessPeriod period = BusinessPeriod.of(start, end);
        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            throw new Exception("Invalid start/end dates");
        }

        PlantInventoryItem plantItem = inventoryItemRepository.findById(plantId).orElse(null);
        if (plantItem == null) {
            throw new Exception("item with plantId " + plantId.toString() + " not found");
        }

        if (description == null) {
            description = "";
        }

        if (site == null) {
            throw new Exception("site cannot be null");
        }

        if (engineerName == null) {
            throw new Exception("engineerName cannot be null");
        }

        BusinessPeriod businessPeriod = BusinessPeriod.of(start, end);

        MaintenanceOrder mo = MaintenanceOrder.of(plantItem, businessPeriod, site, description, engineerName);
        MaintenanceOrder savedMo = maintenanceOrderRepository.save(mo);

        return maintenanceOrderAssembler.toModel(savedMo);
    }

    public MaintenanceOrderDTO acceptMO(Long id) throws Exception {
        MaintenanceOrder mo = maintenanceOrderRepository.findById(id).orElse(null);

        if (mo == null) {
            throw new Exception("Error: could not find maintenance order with id");
        }

        if (mo.getStatus() != MOStatus.PENDING) {
            throw new Exception("Error: to accept Maintenance order previous status should be PENDING");
        }

        mo.setStatus(MOStatus.ACCEPTED);
        maintenanceOrderRepository.save(mo);

        return maintenanceOrderAssembler.toModel(mo);
    }

    public MaintenanceOrderDTO rejectMO(Long id) throws Exception {
        MaintenanceOrder mo = maintenanceOrderRepository.findById(id).orElse(null);

        if (mo == null) {
            throw new Exception("Error: could not find maintenance order with id");
        }

        if (mo.getStatus() != MOStatus.PENDING) {
            throw new Exception("Error: to reject Maintenance order previous status should be PENDING");
        }

        mo.setStatus(MOStatus.REJECTED);
        maintenanceOrderRepository.save(mo);

        return maintenanceOrderAssembler.toModel(mo);
    }

    public MaintenanceOrderDTO cancelMO(Long id) throws Exception {
        MaintenanceOrder mo = maintenanceOrderRepository.findById(id).orElse(null);

        if (mo == null) {
            throw new Exception("Error: could not find maintenance order with id");
        }

        if (LocalDate.now().isBefore(mo.getMaintenancePeriod().getStartDate())) {
            mo.setStatus(MOStatus.CANCELLED);
            maintenanceOrderRepository.save(mo);
        } else {
            throw new Exception("Error: current date must be before maintenance start date");
        }

        return maintenanceOrderAssembler.toModel(mo);
    }

    public MaintenanceOrderDTO completeMO(Long id) throws Exception {
        MaintenanceOrder mo = maintenanceOrderRepository.findById(id).orElse(null);

        if (mo == null) {
            throw new Exception("Error: could not find maintenance order with id");
        }

        if (mo.getStatus() != MOStatus.ACCEPTED) {
            // date check not in requirements.
            throw new Exception("Error: status must be accepted to complete a maintenance order");
        }

        mo.setStatus(MOStatus.COMPLETED);
        maintenanceOrderRepository.save(mo);

        return maintenanceOrderAssembler.toModel(mo);
    }

}
