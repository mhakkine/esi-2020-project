package com.example.rentit.maintenance.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodValidator;
import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.rentit.maintenance.application.dto.MaintenanceTaskCreationBody;
import com.example.rentit.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.rentit.maintenance.domain.model.MaintenanceTask;
import com.example.rentit.maintenance.domain.model.TypeOfWork;
import com.example.rentit.maintenance.domain.repository.MaintenanceTaskRepository;
import com.example.rentit.sales.application.services.PurchaseOrderService;
import com.example.rentit.sales.domain.model.PurchaseOrder;
import com.example.rentit.sales.domain.repository.CustomPurchaseOrderRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Service
public class MaintenanceTaskService {

    @Autowired
    private PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    private MaintenanceTaskRepository maintenanceTaskRepository;

    @Autowired
    private MaintenanceTaskAssembler maintenanceTaskAssembler;

    @Autowired
    private CustomPurchaseOrderRepository customPurchaseOrderRepository;

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    public CollectionModel<MaintenanceTaskDTO> all() {
        List<MaintenanceTask> maintenanceTask = maintenanceTaskRepository.findAll();
        return maintenanceTaskAssembler.toCollectionModel(maintenanceTask);
    }

    public MaintenanceTaskDTO findMaintenanceTask(Long id) throws Exception {
        MaintenanceTask maintenanceTask = maintenanceTaskRepository.findById(id).orElse(null);
        if (maintenanceTask == null) {
            throw new Exception("Maintenance task does not exist");
        }
        return maintenanceTaskAssembler.toModel(maintenanceTask);
    }

    public Pair<String, MaintenanceTaskDTO> createMaintenanceTask(MaintenanceTaskCreationBody body) throws Exception {
        LocalDate start = body.getStartDate();
        LocalDate end = body.getEndDate();
        Long plantId = body.getReservationId();
        String replaced = "";
        TypeOfWork type = body.getType();
        String description = body.getDescription();
        if (start == null) {
            throw new Exception("start cannot be null");
        }

        if (end == null) {
            throw new Exception("end cannot be null");
        }

        BusinessPeriod period = BusinessPeriod.of(start, end);
        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            throw new Exception("Invalid start/end dates");
        }

        PlantInventoryItem plantItem = plantInventoryItemRepository.findById(plantId).orElse(null);
        if (plantItem == null) {
            throw new Exception("item with plantId " + plantId.toString() + " not found");
        }

        if (description == null) {
            description = "";
        }

        BusinessPeriod businessPeriod = BusinessPeriod.of(start, end);
        MaintenanceTask maintenanceTask = new MaintenanceTask();
        maintenanceTask.setReservation(plantItem);
        maintenanceTask.setType_of_work(type);
        maintenanceTask.setPrice(BigDecimal.valueOf((Period.between(start, end).getDays() + 1) * plantItem.getPlantInfo().getPrice().floatValue()));
        maintenanceTask.setMaintenancePeriod(businessPeriod);
        maintenanceTask.setDescription(description);
        MaintenanceTask savedMaintenanceTask = maintenanceTaskRepository.save(maintenanceTask);

        List<PurchaseOrder> purchaseOrders = customPurchaseOrderRepository.GetAllPurchaseOrdersInPeriodForPlant(plantId, start, end);
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            boolean replaced2 = purchaseOrderService.replacePlant(purchaseOrder, plantId);
            if (replaced == "" && replaced2)
                replaced = "The following purchase orders were cancelled: " + purchaseOrder.getId().toString();
            else {
                if (replaced2) {
                    replaced += ", " + purchaseOrder.getId().toString();
                }
            }
        }
        return Pair.of(replaced, maintenanceTaskAssembler.toModel(savedMaintenanceTask));
    }

    public MaintenanceTaskDTO updateMaintenanceTask(MaintenanceTaskDTO taskDTO) throws Exception {
        if (taskDTO.get_id() == null) {
            throw new Exception("Cannot update task without id");
        }
        MaintenanceTask maintenanceTask = maintenanceTaskRepository.findById(taskDTO.get_id()).orElse(null);
        if (taskDTO.get_id() == null) {
            throw new Exception("Cannot update task which does not exist");
        }
        maintenanceTask.setDescription(taskDTO.getDescription());
        maintenanceTask.setMaintenancePeriod(BusinessPeriod.of(taskDTO.getMaintenancePeriod().getStartDate(), taskDTO.getMaintenancePeriod().getEndDate()));
        maintenanceTask.setPrice(BigDecimal.valueOf((Period.between(taskDTO.getMaintenancePeriod().getStartDate(), taskDTO.getMaintenancePeriod().getEndDate()).getDays() + 1) * maintenanceTask.getReservation().getPlantInfo().getPrice().floatValue()));
        maintenanceTask.setType_of_work(taskDTO.getType());
        if (taskDTO.getReservationId() == null) {
            throw new Exception("Cannot update task without reservation id");
        }
        PlantInventoryItem plantInventoryItem = plantInventoryItemRepository.getOne(taskDTO.getReservationId());
        if (plantInventoryItem == null) {
            throw new Exception("Plant inventory item does not exist");
        }
        maintenanceTask.setReservation(plantInventoryItem);
        maintenanceTaskRepository.save(maintenanceTask);
        return maintenanceTaskAssembler.toModel(maintenanceTaskRepository.findById(taskDTO.get_id()).orElse(null));
    }
}