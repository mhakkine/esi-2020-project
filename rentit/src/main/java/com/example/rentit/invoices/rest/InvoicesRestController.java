package com.example.rentit.invoices.rest;

import com.example.rentit.invoices.application.dto.InvoiceDTO;
import com.example.rentit.invoices.application.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;


@RestController
@RequestMapping("/api/invoices")
public class InvoicesRestController {

    @Autowired
    InvoiceService invoiceService;

    @GetMapping("/createByPOid")
    public ResponseEntity createInvoice(@RequestParam("poId") Long poid, @RequestParam("dueDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dueDate) {
        try {
            InvoiceDTO invoiceDTO = invoiceService.createInvoice(poid, dueDate);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(invoiceDTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

            return new ResponseEntity<>(invoiceDTO, headers, HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("")
    public CollectionModel<InvoiceDTO> getAllInvoices() throws Exception {
        try {
            return invoiceService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get invoices", e);
        }
    }

    @PostMapping("/{id}/submitPaymentReminder")
    public ResponseEntity submitPaymentReminder(@PathVariable("invoiceId") Long invoiceId) {
        boolean result = invoiceService.submitPaymentReminder(invoiceId);
        if (result) {
            return ResponseEntity.status(HttpStatus.OK).body("Payment reminded successfully.");
        } else {
            return ResponseEntity.badRequest().body("Can not remind.");
        }
    }

    @PostMapping("/{id}/submitRemittanceAdvice")
    public ResponseEntity submitRemittanceAdvice(@PathVariable("id") Long id) {
        try {
            InvoiceDTO invoice = invoiceService.submitRemittanceAdvice(id);
            return new ResponseEntity<InvoiceDTO>(invoice, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/create")
    public void createInvoiceInBuildit(@RequestBody InvoiceCreationBody creationBody) throws Exception {
        URL url = new URL("http://localhost:8080/api/invoices/create");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);

        String jsonInputString = "{\"purchaseOrderReference\": \"" + creationBody.getPurchaseOrderReference() + "\", \"dueDate\": \"" + creationBody.getDueDate() + "\", \"amount\": \"" + creationBody.getAmount() + "\"}";

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        } catch (Exception ex) {
            throw new Exception(ex);
        }

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

}
