package com.example.rentit.invoices.domain.model;

public enum InvoiceStatus {
    PAID, UNPAID
}
