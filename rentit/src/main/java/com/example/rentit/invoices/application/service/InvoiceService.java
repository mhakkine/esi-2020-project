package com.example.rentit.invoices.application.service;

import com.example.rentit.invoices.application.dto.InvoiceDTO;
import com.example.rentit.invoices.domain.model.Invoice;
import com.example.rentit.invoices.domain.model.InvoiceStatus;
import com.example.rentit.invoices.domain.repository.InvoiceRepository;
import com.example.rentit.sales.domain.model.PurchaseOrder;
import com.example.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Service
public class InvoiceService {

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Value("${buildit.URL.api}")
    String builditURLapi;

    public InvoiceDTO createInvoice(Long purchaseOrderId, LocalDate dueDate) throws Exception {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(purchaseOrderId).orElse(null);

        Invoice invoice = Invoice.of(purchaseOrder, dueDate, purchaseOrder.getTotal());
        invoice.setStatus(InvoiceStatus.UNPAID);
        invoiceRepository.save(invoice);

        purchaseOrder.setInvoice(invoice);
        purchaseOrderRepository.save(purchaseOrder);
        return invoiceAssembler.toModel(invoice);
    }

    public CollectionModel<InvoiceDTO> all() throws Exception {
        return invoiceAssembler.toCollectionModel(invoiceRepository.findAll());
    }

    public boolean submitPaymentReminder(Long invoiceId) {
        Invoice invoice = invoiceRepository.findById(invoiceId).orElse(null);
        if (invoice != null) {
            String url = builditURLapi + "/invoices/paymentReminder/" + invoice.getPurchaseOrder().getId();

            RestTemplate restTemplate = new RestTemplate();
            String response = restTemplate.postForObject(url, null, String.class);
            return Boolean.parseBoolean(response);
        } else {
            return false;
        }
    }

    public InvoiceDTO submitRemittanceAdvice(Long id) throws Exception {
        Invoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice != null) {
            invoice.setStatus(InvoiceStatus.PAID);
            invoiceRepository.save(invoice);
            return invoiceAssembler.toModel(invoice);
        } else {
            throw new Exception("Invoice can not be found by provided id.");
        }
    }
}
