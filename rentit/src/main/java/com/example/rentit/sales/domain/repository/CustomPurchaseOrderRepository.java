package com.example.rentit.sales.domain.repository;

import com.example.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CustomPurchaseOrderRepository {
    public List<PurchaseOrder> GetAllPurchaseOrdersInPeriodForPlant(Long id, LocalDate start, LocalDate end);
}
