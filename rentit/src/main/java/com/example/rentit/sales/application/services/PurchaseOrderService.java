package com.example.rentit.sales.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodValidator;
import com.example.rentit.common.application.exception.PlantNotFoundException;
import com.example.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.example.rentit.inventory.application.service.PlantInventoryEntryService;
import com.example.rentit.inventory.application.service.PlantInventoryItemAvailabilityService;
import com.example.rentit.inventory.application.service.PlantInventoryItemService;
import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.domain.model.PlantReservation;
import com.example.rentit.inventory.domain.repository.CustomPlantReservationRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository2;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.rentit.inventory.domain.repository.PlantReservationRepository;
import com.example.rentit.invoices.application.service.InvoiceService;
import com.example.rentit.sales.application.dto.PurchaseOrderDTO;
import com.example.rentit.sales.domain.model.POStatus;
import com.example.rentit.sales.domain.model.PurchaseOrder;
import com.example.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class PurchaseOrderService {

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    CustomPlantReservationRepository customPlantReservationRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    PlantInventoryEntryService plantInventoryEntryService;

    @Autowired
    PlantInventoryItemService plantInventoryItemService;

    @Autowired
    PlantInventoryItemAvailabilityService plantInventoryItemAvailabilityService;

    @Autowired
    PlantInventoryEntryRepository2 plantInventoryEntryRepo2;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    InvoiceService invoiceService;

    public PurchaseOrderDTO createPO(LocalDate startDate, LocalDate endDate, Long plantId) throws Exception {
        BusinessPeriod period = BusinessPeriod.of(startDate, endDate);

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid Interval");

        if (plantInventoryEntryService.findEntryById(plantId) == null)
            throw new Exception("Invalid Plant Inventory");

        PlantInventoryEntry plant = plantInventoryEntryService.findEntryById(plantId);

        if (plant == null)
            throw new Exception("Plant NOT Found");

        PurchaseOrder po = PurchaseOrder.of(plant, period);

        Iterator<PlantInventoryItemDTO> availableItems = plantInventoryItemService.findAvailableItems(
                plant.getName(),
                startDate,
                endDate).iterator();

        if (!availableItems.hasNext()) {
            po.setStatus(POStatus.REJECTED);
            throw new Exception("NO available items");
        }

        PlantInventoryItem item = plantInventoryItemService.findItemById(availableItems.next().get_id());

        PlantReservation reservation = PlantReservation.of(item, po.getRentalPeriod());
        purchaseOrderRepository.save(po);
        reservation.setRental(po);
        plantReservationRepository.save(reservation);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO modifyPO(LocalDate startDate, LocalDate endDate, long purchaseOrderID) throws Exception {
        BusinessPeriod period = BusinessPeriod.of(startDate, endDate);

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid Interval");

        PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(purchaseOrderID).orElse(null);
        if (purchaseOrder == null) {
            throw new Exception("Purchase order does not exist");
        }
        if (purchaseOrder.getStatus() == POStatus.PLANT_RETURNED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is already returned, id: " + purchaseOrderID);
        }
        if (purchaseOrder.getStatus() == POStatus.REJECTED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is rejected, id: " + purchaseOrderID);
        }
        if (purchaseOrder.getStatus() == POStatus.REJECTED_BY_CUSTOMER) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is rejected by customer, id: " + purchaseOrderID);
        }
        if (purchaseOrder.getStatus() == POStatus.CANCELLED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is cancelled, id: " + purchaseOrderID);
        }
        if (purchaseOrder.getStatus() == POStatus.PLANT_DELIVERED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is delivered, id: " + purchaseOrderID);
        }
        if (purchaseOrder.getStatus() == POStatus.PLANT_DISPATCHED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is dispatched, id: " + purchaseOrderID);
        }
        List<Long> reservationIDs = new ArrayList<Long>();
        List<PlantReservation> reservations = customPlantReservationRepository.getPlantReservationsByPurchaseOrderId(purchaseOrderID);
        for (PlantReservation reservation : reservations) {
            reservationIDs.add(reservation.getId());
        }

        Iterator<PlantInventoryItemDTO> availableItems = plantInventoryItemService.findAvailableItemsSkipExisting(
                reservationIDs,
                purchaseOrder.getPlant().getName(),
                startDate,
                endDate).iterator();

        if (!availableItems.hasNext()) {
            purchaseOrder.setStatus(POStatus.REJECTED);
            purchaseOrderRepository.save(purchaseOrder);
            throw new Exception("NO available items");
        }
        purchaseOrder.setRentalPeriod(BusinessPeriod.of(startDate, endDate));
        purchaseOrderRepository.save(purchaseOrder);
        for (PlantReservation reservation : reservations) {
            reservation.setSchedule(BusinessPeriod.of(startDate, endDate));
            plantReservationRepository.save(reservation);
        }
        return purchaseOrderAssembler.toModel(purchaseOrder);
    }

    public CollectionModel<PurchaseOrderDTO> all() {
        return purchaseOrderAssembler.toCollectionModel(purchaseOrderRepository.findAll());
    }

    public PurchaseOrderDTO findPO(Long id) {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO dispatchPO(Long id) throws Exception {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);

        if (po.getStatus() == POStatus.ACCEPTED) {
            po.setStatus(POStatus.PLANT_DISPATCHED);
        }

        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO cancelPO(Long id) throws Exception {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        POStatus currentStatus = po.getStatus();

        if (currentStatus == POStatus.ACCEPTED || currentStatus == POStatus.PENDING) {
            po.setStatus(POStatus.CANCELLED);
        }

        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO rejectPO(Long id, boolean byCustomer) throws Exception {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        validatePO(po);
        if (po.getStatus() != POStatus.PENDING && !byCustomer)
            throw new Exception("PO cannot be rejected due to it is not Pending");
        if (po.getStatus() != POStatus.PLANT_DISPATCHED && byCustomer)
            throw new Exception("PO cannot be rejected by customer if it has not been dispatched");
        if (!customPlantReservationRepository.getPlantReservationsByPurchaseOrderId(po.getId()).isEmpty())
            plantReservationRepository.deleteAll(customPlantReservationRepository.getPlantReservationsByPurchaseOrderId(po.getId()));
        if (byCustomer) {
            po.setStatus(POStatus.REJECTED_BY_CUSTOMER);
        } else {
            po.setStatus(POStatus.REJECTED);
        }
        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO returnPO(Long id) throws Exception {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        validatePO(po);
        if (po.getStatus() != POStatus.PLANT_DELIVERED)
            throw new Exception("PO cannot be returned, as it is not delivered");
        if (customPlantReservationRepository.getPlantReservationsByPurchaseOrderId(id).size() > 0) {
            List<PlantReservation> plantReservations = customPlantReservationRepository.getPlantReservationsByPurchaseOrderId(id);
            for (PlantReservation plantReservation : plantReservations) {
                if (plantReservation.getSchedule().getEndDate().isAfter(LocalDate.now())) {
                    PlantReservation pr = plantReservationRepository.findById(plantReservation.getId()).orElse(null);
                    if (pr == null)
                        continue;
                    pr.setSchedule(BusinessPeriod.of(pr.getSchedule().getStartDate(), LocalDate.now()));
                    plantReservationRepository.save(pr);
                }
            }
        }
        invoiceService.createInvoice(id, LocalDate.now().plusDays(30));
        po.setStatus(POStatus.PLANT_RETURNED);
        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO markPOAsDelivered(Long id) throws Exception {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        validatePO(po);
        if (po.getStatus() != POStatus.PLANT_DISPATCHED)
            throw new Exception("PO cannot be marked as delivered, as it is not dispatched");
        po.setStatus(POStatus.PLANT_DELIVERED);
        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO acceptPO(Long id) throws Exception {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        validatePO(po);
        if (po.getStatus() != POStatus.PENDING)
            throw new Exception("PO cannot be accepted due to it is not Pending");
        po.setStatus(POStatus.ACCEPTED);
        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    private void validatePO(PurchaseOrder po) throws Exception {
        if (po == null)
            throw new Exception("PO Not Found");
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
    }

    public boolean extendPO(LocalDate extendEndDate, Long purchaseOrderId) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(purchaseOrderId).orElse(null);
        if (purchaseOrder.getStatus() == POStatus.PLANT_RETURNED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is already returned, id: " + purchaseOrderId);
        }
        if (purchaseOrder.getStatus() == POStatus.REJECTED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is rejected, id: " + purchaseOrderId);
        }
        if (purchaseOrder.getStatus() == POStatus.REJECTED_BY_CUSTOMER) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is rejected by customer, id: " + purchaseOrderId);
        }
        if (purchaseOrder.getStatus() == POStatus.CANCELLED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PO cannot be extended,as it is cancelled, id: " + purchaseOrderId);
        }
        if (purchaseOrder == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: purchase order not found by id " + purchaseOrderId);
        }
        PlantInventoryEntry entry = plantInventoryEntryRepo2.findById(purchaseOrder.getPlant().getId()).orElse(null);
        if (entry == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: plant inventory entry not found by id " + purchaseOrder.getPlant().getId());
        }
        PlantInventoryItem item = plantInventoryItemRepository.findOneByPlantInfo(entry);

        LocalDate newEndDate = extendEndDate;
        if (newEndDate.isBefore(purchaseOrder.getRentalPeriod().getEndDate())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: new endDate can not be before existing endDate");
        }
        boolean isAvailable = false;
        try {
            isAvailable = plantInventoryItemAvailabilityService.isAvailable(item.getId(), purchaseOrder.getRentalPeriod().getEndDate().plusDays(1), newEndDate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get availability information", e);
        }

        if (isAvailable) {
            PlantReservation plantReservation = customPlantReservationRepository.getPlantReservationByPurchaseOrderId(purchaseOrder.getId());
            plantReservation.setSchedule(BusinessPeriod.of(plantReservation.getSchedule().getStartDate(), extendEndDate));
            BigDecimal cost = entry.getPrice().multiply(BigDecimal.valueOf(DAYS.between(plantReservation.getSchedule().getStartDate(), newEndDate)));
            purchaseOrder.setTotal(cost);
            purchaseOrder.setRentalPeriod(BusinessPeriod.of(purchaseOrder.getRentalPeriod().getStartDate(), newEndDate));
            purchaseOrderRepository.save(purchaseOrder);
            plantReservationRepository.save(plantReservation);
            return true;
        } else {
            return false;
        }
    }

    public boolean replacePlant(PurchaseOrder purchaseOrder, Long plantId) {
        PlantInventoryItem item = plantInventoryItemRepository.findItem(plantId);

        List<PlantReservation> reservations = customPlantReservationRepository.getPlantReservationsByPurchaseOrderId(purchaseOrder.getId());
        for (PlantReservation reservation : reservations) {
            List<PlantInventoryItem> plants = plantInventoryItemService.findAvailableItemsE(item.getPlantInfo().getName(), purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getStartDate());
            plants.remove(plantId);
            if (plants.size() > 0) {
                reservation.setPlant(plants.get(0));
                purchaseOrder.setPlant(plants.get(0).getPlantInfo());
                purchaseOrder.setStatus(POStatus.PENDING);
            } else {
                purchaseOrder.setStatus(POStatus.CANCELLED);
            }
            purchaseOrderRepository.save(purchaseOrder);
            reservation.setRental(purchaseOrder);
            plantReservationRepository.save(reservation);
        }
        if (reservations.size() < 1) {
            purchaseOrder.setStatus(POStatus.CANCELLED);
            purchaseOrderRepository.save(purchaseOrder);
        }
        return (purchaseOrder.getStatus() == POStatus.CANCELLED);
    }
}
