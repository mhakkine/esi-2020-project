package com.example.rentit.sales.domain.model;

import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.invoices.domain.model.Invoice;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;
    LocalDate paymentSchedule;
    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @OneToOne
    Invoice invoice;

    public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = entry;
        po.rentalPeriod = period;
        po.issueDate = LocalDate.now();
        po.status = POStatus.PENDING;
        return po;
    }

}
