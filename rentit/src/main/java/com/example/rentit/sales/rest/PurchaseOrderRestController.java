package com.example.rentit.sales.rest;

import com.example.rentit.sales.application.dto.PurchaseOrderDTO;
import com.example.rentit.sales.application.services.PurchaseOrderService;
import com.example.rentit.sales.domain.model.POExtensionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;


@RestController
@RequestMapping("/api/sales")
public class PurchaseOrderRestController {

    @Autowired
    PurchaseOrderService purchaseOrderService;

    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public CollectionModel<PurchaseOrderDTO> fetchPurchaseOrders() {
        try {
            return purchaseOrderService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get purchase orders", e);
        }
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        try {
            return purchaseOrderService.findPO(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error: could not find purchase order", e);
        }
    }

    @PostMapping("/orders")
    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody CreationBody creationBody) throws Exception {
        try {
            PurchaseOrderDTO newlyCreatePODTO = purchaseOrderService.createPO(
                    creationBody.getStartDate(),
                    creationBody.getEndDate(),
                    creationBody.getPlantId()
            );

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(newlyCreatePODTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

            return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PatchMapping("/orders/{id}/modify")
    public ResponseEntity<PurchaseOrderDTO> modifyPurchaseOrder(
            @PathVariable("id") Long id,
            @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate newStartDate,
            @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate newEndDate) throws ResponseStatusException {
        try {
            PurchaseOrderDTO modifiedPO = purchaseOrderService.modifyPO(newStartDate, newEndDate, id);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(modifiedPO.getRequiredLink(IanaLinkRelations.SELF).toUri());
            return new ResponseEntity<>(modifiedPO, headers, HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: something went wrong" + " " + e.getMessage(), e);
        }
    }

    @PostMapping("/orders/{id}/dispatch")
    public PurchaseOrderDTO dispatchPurchaseOrder(@PathVariable Long id) throws Exception {
        return purchaseOrderService.dispatchPO(id);
    }

    @PostMapping("/orders/{id}/cancel")
    public PurchaseOrderDTO cancelPurchaseOrder(@PathVariable Long id) throws Exception {
        return purchaseOrderService.cancelPO(id);
    }

    @PostMapping("/orders/{id}/accept")
    public PurchaseOrderDTO acceptPurchaseOrder(@PathVariable Long id) {
        try {
            return purchaseOrderService.acceptPO(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("orders/{id}/reject")
    public PurchaseOrderDTO rejectPurchaseOrder(@PathVariable Long id) {
        try {
            return purchaseOrderService.rejectPO(id, false);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("orders/{id}/reject-by-customer")
    public PurchaseOrderDTO rejectPurchaseOrderByCustomer(@PathVariable Long id) {
        try {
            return purchaseOrderService.rejectPO(id, true);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("orders/{id}/mark-as-delivered")
    public PurchaseOrderDTO markPurchaseOrderAsDelivered(@PathVariable Long id) {
        try {
            return purchaseOrderService.markPOAsDelivered(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("orders/{id}/return")
    public PurchaseOrderDTO returnPurchaseOrder(@PathVariable Long id) {
        try {
            return purchaseOrderService.returnPO(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/orders/{id}/extensions")
    public CollectionModel<EntityModel<POExtensionDTO>> retrievePurchaseOrderExtensions(@PathVariable("id") Long id) {
        List<EntityModel<POExtensionDTO>> result = new ArrayList<>();
        POExtensionDTO extension = new POExtensionDTO();
        extension.setEndDate(LocalDate.now().plusWeeks(1));

        result.add(new EntityModel<>(extension));
        return new CollectionModel<>(result,
                linkTo(methodOn(PurchaseOrderRestController.class).retrievePurchaseOrderExtensions(id))
                        .withSelfRel()
                        .andAffordance(afford(methodOn(PurchaseOrderRestController.class).requestPurchaseOrderExtension(null, id))));
    }

    @PostMapping("/orders/{id}/extensions")
    public EntityModel<?> requestPurchaseOrderExtension(@RequestBody POExtensionDTO extension, @PathVariable("id") Long id) {
        // Add code to handle the extension of the purchase order
        return null;
    }

    @PatchMapping("/orders/{id}/extend")
    public ResponseEntity<Boolean> requestPOExtension(
            @PathVariable("id") Long id,
            @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate extendEndDate) {

        boolean isExtended = false;
        try {
            isExtended = purchaseOrderService.extendPO(extendEndDate, id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: something went wrong", e);
        }
        if (isExtended) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.ok(false);
        }
    }
}
