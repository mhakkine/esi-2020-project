package com.example.rentit.sales.application.dto;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.invoices.application.dto.InvoiceDTO;
import com.example.rentit.sales.domain.model.POStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;


@Data
public class PurchaseOrderDTO extends RepresentationModel<PurchaseOrderDTO> {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    Long plantId;
    POStatus status;
    InvoiceDTO invoice;
}
