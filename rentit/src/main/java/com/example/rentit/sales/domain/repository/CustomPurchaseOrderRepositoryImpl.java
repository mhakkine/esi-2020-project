package com.example.rentit.sales.domain.repository;

import com.example.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

@Service
public class CustomPurchaseOrderRepositoryImpl implements CustomPurchaseOrderRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<PurchaseOrder> GetAllPurchaseOrdersInPeriodForPlant(Long id, LocalDate start, LocalDate end) {
        return em.createQuery("select p from PurchaseOrder p where p.plant.id = ?1 and not ((p.rentalPeriod.startDate > ?3 and p.rentalPeriod.endDate > ? 3) or (p.rentalPeriod.startDate < ?2 and p.rentalPeriod.endDate < ? 2)) ",
                PurchaseOrder.class)
                .setParameter(1, id)
                .setParameter(2, start)
                .setParameter(3, end)
                .getResultList();
    }
}
