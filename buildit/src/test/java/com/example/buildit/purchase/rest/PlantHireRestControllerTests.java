package com.example.buildit.purchase.rest;

import com.example.buildit.BuildItApplication;
import com.example.buildit.purchase.application.dto.PlantHireRequestDTO;
import com.example.buildit.purchase.domain.model.RequestStatus;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BuildItApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantHireRestControllerTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    // CC1
    @Test
    public void testCreatePlantHireRequest() throws Exception {
        PlantHireRestCreationBody body = new PlantHireRestCreationBody();
        body.setStartDate(LocalDate.of(2020, 5, 1));
        body.setEndDate(LocalDate.of(2020, 6, 1));
        body.setSiteId("1");
        body.setStatus(RequestStatus.PENDING);
        body.setPrice(BigDecimal.valueOf(30));
        body.setSupplier("Supplier");
        body.setSiteEngineerName("Engineer Name");


        mockMvc.perform(post("/api/plantHire/requests")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        assertThat(checkRequests().size()).isEqualTo(9);
    }

    // CC2
    @Test
    public void testEditPlantHireRequest() throws Exception {
        PlantHireRestCreationBody body = new PlantHireRestCreationBody();
        body.setStartDate(LocalDate.of(2020, 5, 1));
        body.setEndDate(LocalDate.of(2020, 6, 1));
        body.setSiteId("1");
        body.setStatus(RequestStatus.PENDING);
        body.setPrice(BigDecimal.valueOf(30));
        body.setSupplier("Supplier");
        body.setSiteEngineerName("Engineer Name");


        mockMvc.perform(post("/api/plantHire/requests")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        PlantHireRestCreationBody newBody = new PlantHireRestCreationBody();
        newBody.setStartDate(LocalDate.of(2020, 5, 1));
        newBody.setEndDate(LocalDate.of(2020, 6, 1));
        newBody.setSiteId("34");
        newBody.setStatus(RequestStatus.PENDING);
        newBody.setPrice(BigDecimal.valueOf(30));
        newBody.setSupplier("Supplier 2");
        newBody.setSiteEngineerName("Engineer Name 2");

        mockMvc.perform(patch("/api/plantHire/requests/1")
                .content(mapper.writeValueAsString(newBody))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        System.out.println(checkRequests().get(0).getSiteEngineerName());
        assertThat(checkRequests().size()).isEqualTo(9);
        assertThat(checkRequests().get(0).getSiteEngineerName()).isEqualTo("Engineer Name 2");
        assertThat(checkRequests().get(0).getSupplier()).isEqualTo("Supplier 2");
        assertThat(checkRequests().get(0).getSiteId()).isEqualTo("34");
    }

    @Test
    public void testGetAllPlantHires() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/plantHire/requests").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("plantHireRequestDToes");
        List<PlantHireRequestDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PlantHireRequestDTO>>() {
        });

        assertThat(plantHireRequests.size()).isEqualTo(8);
    }


    @Test
    public void testPlantHireWithWrongIdReturnsNotFound() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/99999/approve").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testPendingPlantHireCanBeCancelled() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/1/cancel").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    // CC3
    @Test
    public void testCancelledPlantHireCanNotBeCancelledAgain() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/5/cancel").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    // CC3
    @Test
    public void testPlantHireCanNotBeCancelledIfStartDateBeforeToday() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/7/cancel").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    // CC5
    @Test
    public void testPendingPlantHireCanBeApproved() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/1/approve").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    // CC5
    @Test
    public void testCancelledPlantHireCanNotBeApproved() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/5/approve").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    // CC5
    @Test
    public void testPendingPlantHireCanBeRejected() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/1/reject").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    // CC5
    @Test
    public void testCancelledPlantHireCanNotBeRejected() throws Exception {
        mockMvc.perform(post("/api/plantHire/requests/5/reject").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    public List<PlantHireRequestDTO> checkRequests() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/plantHire/requests").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode plantHiresJSON = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("plantHireRequestDToes");
        List<PlantHireRequestDTO> plantHireRequests = mapper.readValue(plantHiresJSON.toString(), new TypeReference<List<PlantHireRequestDTO>>() {
        });
        return plantHireRequests;
    }
}
