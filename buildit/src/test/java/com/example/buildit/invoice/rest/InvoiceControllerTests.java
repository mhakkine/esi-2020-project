package com.example.buildit.invoice.rest;

import com.example.buildit.BuildItApplication;
import com.example.buildit.invoices.application.dto.CreateInvoiceRequestDTO;
import com.example.buildit.invoices.application.dto.InvoiceDTO;
import com.example.buildit.invoices.application.services.InvoiceService;
import com.example.buildit.invoices.domain.model.InvoiceStatus;
import com.example.buildit.purchase.application.dto.PurchaseOrderDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BuildItApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InvoiceControllerTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    InvoiceService invoiceService;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    // CC9
    @Test
    public void testCreateInvoice() throws Exception {
        CreateInvoiceRequestDTO body = new CreateInvoiceRequestDTO();
        body.setAmount(new BigDecimal("250.0"));
        body.setDueDate(LocalDate.of(2020, 6, 1));
        body.setPurchaseOrderReference("1");

        mockMvc.perform(post("/api/invoices/create")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        assertThat(checkInvoices().size()).isEqualTo(1);
    }

    // CC10
    public List<InvoiceDTO> checkInvoices() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/invoices").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode invoicesJSON = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("invoiceDToes");
        List<InvoiceDTO> invoices = mapper.readValue(invoicesJSON.toString(), new TypeReference<List<InvoiceDTO>>() {
        });
        return invoices;
    }

    // CC11 Test
    @Test
    public void testApproveInvoice() throws Exception {
        CreateInvoiceRequestDTO body = new CreateInvoiceRequestDTO();
        body.setAmount(new BigDecimal("250.0"));
        body.setDueDate(LocalDate.of(2020, 6, 1));
        body.setPurchaseOrderReference("1");

        mockMvc.perform(post("/api/invoices/create")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        assertThat(checkInvoices().size()).isEqualTo(1);

        MvcResult result = mockMvc.perform(post("/api/invoices/1/accept")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        InvoiceDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), InvoiceDTO.class);
        assertEquals(InvoiceStatus.APPROVED, returnedOrder.getStatus());
    }

    // CC11 Test
    @Test
    public void testRejectInvoice() throws Exception {
        CreateInvoiceRequestDTO body = new CreateInvoiceRequestDTO();
        body.setAmount(new BigDecimal("250.0"));
        body.setDueDate(LocalDate.of(2020, 6, 1));
        body.setPurchaseOrderReference("1");

        mockMvc.perform(post("/api/invoices/create")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        assertThat(checkInvoices().size()).isEqualTo(1);

        MvcResult result = mockMvc.perform(post("/api/invoices/1/reject")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        InvoiceDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), InvoiceDTO.class);
        assertEquals(InvoiceStatus.REJECTED, returnedOrder.getStatus());
    }

    @Test
    public void testCheckPOInReceivedInvoice() throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO();
        InvoiceDTO invoice = new InvoiceDTO();
        invoice.setStatus(InvoiceStatus.PAID);
        purchaseOrderDTO.setInvoice(invoice);
        assertFalse(invoiceService.isPOUnpaid(purchaseOrderDTO));
    }

    // CC12 Test
    @Test
    public void testInvoiceRemittance() throws Exception {
        CreateInvoiceRequestDTO body = new CreateInvoiceRequestDTO();
        body.setAmount(new BigDecimal("250.0"));
        body.setDueDate(LocalDate.of(2020, 6, 1));
        body.setPurchaseOrderReference("1");

        mockMvc.perform(post("/api/invoices/create")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/api/invoices/1/accept")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        MvcResult result = mockMvc.perform(post("/api/invoices/1/remittance")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        InvoiceDTO returnedOrder = mapper.readValue(result.getResponse().getContentAsString(), InvoiceDTO.class);
        assertEquals(InvoiceStatus.PAID, returnedOrder.getStatus());
        assertEquals(LocalDate.now(), returnedOrder.getPaymentDate());
    }

    // CC12 Test
    @Test
    public void testInvoiceRemittanceError() {
        try {
            CreateInvoiceRequestDTO body = new CreateInvoiceRequestDTO();
            body.setAmount(new BigDecimal("250.0"));
            body.setDueDate(LocalDate.of(2020, 6, 1));
            body.setPurchaseOrderReference("1");

            mockMvc.perform(post("/api/invoices/create")
                    .content(mapper.writeValueAsString(body))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated());

            mockMvc.perform(post("/api/invoices/1/remittance")
                    .content(mapper.writeValueAsString(body))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andReturn();
        } catch (Exception ex) {
            assertEquals("Invoice status should be approved", ex.getMessage());
        }
    }

    @Test
    public void testPaymentReminder() throws Exception {
        CreateInvoiceRequestDTO body = new CreateInvoiceRequestDTO();
        body.setAmount(new BigDecimal("250.0"));
        body.setDueDate(LocalDate.of(2020, 6, 1));
        body.setPurchaseOrderReference("1");

        mockMvc.perform(post("/api/invoices/create")
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        List<InvoiceDTO> invoices = checkInvoices();

        mockMvc.perform(post("/api/invoices/paymentReminder/" + invoices.get(0).getPurchaseOrderReference())
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        invoices = checkInvoices();
        assertEquals(InvoiceStatus.REMINDED, invoices.get(0).getStatus());
    }
}
