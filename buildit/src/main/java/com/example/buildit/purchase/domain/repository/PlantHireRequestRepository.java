package com.example.buildit.purchase.domain.repository;

import com.example.buildit.purchase.domain.model.PlantHireRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantHireRequestRepository extends JpaRepository<PlantHireRequest, Long> {
    @Query("select p from PlantHireRequest p where p.purchaseOrderReference = ?1")
    List<PlantHireRequest> findPlantRequestsByPOReference(String poReference);
}
