package com.example.buildit.purchase.application.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ExtensionRequestDTO {

    LocalDate extensionDate;
    Long existingPlantRequestId;

}
