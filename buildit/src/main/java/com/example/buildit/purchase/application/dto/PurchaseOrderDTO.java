package com.example.buildit.purchase.application.dto;


import com.example.buildit.common.application.dto.BusinessPeriodDTO;
import com.example.buildit.invoices.application.dto.InvoiceDTO;
import com.example.buildit.purchase.domain.model.POStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class PurchaseOrderDTO extends RepresentationModel<PurchaseOrderDTO> {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    Long plantId;
    POStatus status;
    InvoiceDTO invoice;
}
