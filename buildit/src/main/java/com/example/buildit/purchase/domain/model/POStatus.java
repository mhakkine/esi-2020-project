package com.example.buildit.purchase.domain.model;

public enum POStatus {PENDING, ACCEPTED, REJECTED, PLANT_DISPATCHED, PLANT_RETURNED, INVOICED, CANCELLED}
