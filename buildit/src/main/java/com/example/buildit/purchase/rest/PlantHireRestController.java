package com.example.buildit.purchase.rest;

import com.example.buildit.purchase.application.dto.ExtensionRequestDTO;
import com.example.buildit.purchase.application.dto.PlantHireRequestDTO;
import com.example.buildit.purchase.application.service.PlantHireRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/plantHire/requests")
public class PlantHireRestController {

    @Value("${rentit.URL.api}")
    String rentitURLapi;

    @Autowired
    PlantHireRequestService plantHireRequestService;

    @GetMapping("")
    public CollectionModel<PlantHireRequestDTO> getAllPlantHireRequests() throws Exception {
        try {
            return plantHireRequestService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get plant hire requests", e);
        }
    }

    @GetMapping("/{id}")
    public PlantHireRequestDTO getPlantHireRequest(@PathVariable Long id) throws Exception {
        try {
            return plantHireRequestService.find(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not find plant hire request with id", e);
        }
    }

    @PostMapping("")
    public ResponseEntity<PlantHireRequestDTO> createPlantHireRequest(@RequestBody PlantHireRestCreationBody plantHireRestCreationBody) throws Exception {
        PlantHireRequestDTO newlyCreatedPlantHireRequestDTO = plantHireRequestService.create(
                plantHireRestCreationBody.getStartDate(),
                plantHireRestCreationBody.getEndDate(),
                plantHireRestCreationBody.getReference(),
                plantHireRestCreationBody.getSiteId(),
                plantHireRestCreationBody.getSiteEngineerName(),
                plantHireRestCreationBody.getSupplier(),
                plantHireRestCreationBody.getStatus(),
                plantHireRestCreationBody.getPrice()

        );

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(newlyCreatedPlantHireRequestDTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

        return new ResponseEntity<>(newlyCreatedPlantHireRequestDTO, headers, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<PlantHireRequestDTO> upadtePlantHireRequest(@PathVariable(value = "id") Long id, @RequestBody PlantHireRestCreationBody plantHireRestCreationBody) throws Exception {
        PlantHireRequestDTO newlyCreatedPlantHireRequestDTO = plantHireRequestService.update(
                id,
                plantHireRestCreationBody.getStartDate(),
                plantHireRestCreationBody.getEndDate(),
                plantHireRestCreationBody.getReference(),
                plantHireRestCreationBody.getSiteId(),
                plantHireRestCreationBody.getSiteEngineerName(),
                plantHireRestCreationBody.getSupplier(),
                plantHireRestCreationBody.getStatus(),
                plantHireRestCreationBody.getPrice()

        );

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(newlyCreatedPlantHireRequestDTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

        return new ResponseEntity<>(newlyCreatedPlantHireRequestDTO, headers, HttpStatus.CREATED);
    }

    @PostMapping("/{id}/approve")
    public PlantHireRequestDTO approvePlantHireRequest(@PathVariable Long id) throws Exception {
        return plantHireRequestService.approve(id);
    }

    @PostMapping("/{id}/reject")
    public PlantHireRequestDTO rejectPlantHireRequest(@PathVariable Long id) throws Exception {
        return plantHireRequestService.reject(id);
    }

    @PostMapping("/{id}/cancel")
    public PlantHireRequestDTO cancelPlantHireRequest(@PathVariable Long id) throws Exception {
        return plantHireRequestService.cancel(id);
    }

    @PostMapping("/extend")
    public ResponseEntity extendPlant(@RequestBody ExtensionRequestDTO extensionRequest) throws Exception {
        PlantHireRequestDTO planRequest = null;
        try {
            planRequest = plantHireRequestService.find(extensionRequest.getExistingPlantRequestId());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("ERROR: Plant Request Id " + extensionRequest.getExistingPlantRequestId() + " does not exist.");
        }
        String url = rentitURLapi + "/sales/orders/add/extension?" +
                "&extendToDate=" + extensionRequest.getExtensionDate() +
                "&startDate=" + planRequest.getRentalPeriod().getStartDate() +
                "&endDate=" + planRequest.getRentalPeriod().getEndDate() +
                "&plantReferenceId=" + planRequest.getPlantReference().substring("/plant/url/".length());
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);
        if (Boolean.parseBoolean(response)) {
            plantHireRequestService.update(planRequest.get_id(),
                    planRequest.getRentalPeriod().getStartDate(),
                    extensionRequest.getExtensionDate(),
                    planRequest.getPlantReference(),
                    planRequest.getSiteId(),
                    planRequest.getSiteEngineerName(),
                    planRequest.getSupplier(),
                    planRequest.getStatus(),
                    planRequest.getPrice());
            return ResponseEntity.ok(response).status(HttpStatus.OK).body("Requested plant was extended.");
        } else {
            return ResponseEntity.ok(response).status(HttpStatus.OK).body("It is not possible to extend requested plant.");
        }
    }

}
