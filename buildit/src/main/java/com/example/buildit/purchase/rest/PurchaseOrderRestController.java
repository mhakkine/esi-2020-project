package com.example.buildit.purchase.rest;


import com.example.buildit.purchase.application.dto.PurchaseOrderDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/plantHire/")
public class PurchaseOrderRestController {

    @PostMapping("/order")
    public String createPurchaseOrder(@RequestBody POCreationBody creationBody) throws Exception {
        URL url = new URL("http://localhost:8090/api/sales/orders");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);

        String jsonInputString = "{\"plantId\": \"" + creationBody.getPlantId() + "\", \"startDate\": \"" + creationBody.getStartDate() + "\", \"endDate\": \"" + creationBody.getEndDate() + "\"}";

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return response.toString();
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @PostMapping("/order/{id}/accept")
    public String acceptPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost("http://localhost:8090/api/sales/orders/" + id + "/accept");

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new Exception(response.getStatusLine().getReasonPhrase());
            } else {
                return "Success";
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

    }

    @PostMapping("/order/{id}/reject")
    public String rejectPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost("http://localhost:8090/api/sales/orders/" + id + "/reject");

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new Exception(response.getStatusLine().getReasonPhrase());
            } else {
                return "success";
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @PostMapping("/order/{id}/reject-by-customer")
    public String rejectPurchaseOrderByCustomer(@PathVariable Long id) throws Exception {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost("http://localhost:8090/api/sales/orders/" + id + "/reject-by-customer");

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new Exception(response.getStatusLine().getReasonPhrase());
            } else {
                return "success";
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @PostMapping("/order/{id}/cancel")
    public String cancelPurchaseOrderByCustomer(@PathVariable Long id) throws Exception {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost("http://localhost:8090/api/sales/orders/" + id + "/cancel");

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new Exception(response.getStatusLine().getReasonPhrase());
            } else {
                return "success";
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @PostMapping("/order/{id}/extend")
    public String extendPurchaseOrderByCustomer(
            @PathVariable Long id,
            @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate extendEndDate
    ) throws Exception {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPatch httpPatch = new HttpPatch("http://localhost:8090/api/sales/orders/" + id + "/extend?to=" + extendEndDate.toString());

            HttpResponse response = httpclient.execute(httpPatch);
            HttpEntity entity = response.getEntity();

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new Exception(response.getStatusLine().getReasonPhrase());
            } else {
                return entity.toString();
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @GetMapping("/order/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) throws IOException {
        String url = "http://localhost:8090/api/sales/orders/" + id;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String readAPIResponse;
        StringBuilder jsonString = new StringBuilder();
        while ((readAPIResponse = in.readLine()) != null) {
            jsonString.append(readAPIResponse);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(jsonString);
        JsonNode poJSON = objectMapper.readTree(jsonString.toString());
        List<PurchaseOrderDTO> invoices = objectMapper.readValue(poJSON.toString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        return invoices.get(0);
    }

}

