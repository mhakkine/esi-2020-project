package com.example.buildit.purchase.application.service;

import com.example.buildit.common.domain.model.BusinessPeriod;
import com.example.buildit.purchase.application.dto.PlantHireRequestDTO;
import com.example.buildit.purchase.domain.model.PlantHireRequest;
import com.example.buildit.purchase.domain.model.RequestStatus;
import com.example.buildit.purchase.domain.repository.PlantHireRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class PlantHireRequestService {

    @Autowired
    PlantHireRequestRepository plantHireRequestRepository;

    @Autowired
    PlantHireRequestAssembler plantHireRequestAssembler;

    public CollectionModel<PlantHireRequestDTO> all() throws Exception {
        return plantHireRequestAssembler.toCollectionModel(plantHireRequestRepository.findAll());
    }

    public PlantHireRequestDTO find(Long id) throws Exception {
        return plantHireRequestAssembler.toModel(plantHireRequestRepository.findById(id).orElse(null));
    }

    public PlantHireRequestDTO approve(Long id) throws Exception {
        PlantHireRequest plantHireRequest = plantHireRequestRepository.findById(id).orElse(null);

        if (plantHireRequest == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find plant hire request with id");
        }

        if (plantHireRequest.getStatus() != RequestStatus.PENDING) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Plant hire request must be in PENDING status to approve");
        }

        plantHireRequest.setStatus(RequestStatus.APPROVED);
        plantHireRequestRepository.save(plantHireRequest);

        return plantHireRequestAssembler.toModel(plantHireRequest);
    }

    public PlantHireRequestDTO reject(Long id) throws Exception {
        PlantHireRequest plantHireRequest = plantHireRequestRepository.findById(id).orElse(null);

        if (plantHireRequest == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find plant hire request with id");
        }

        if (plantHireRequest.getStatus() != RequestStatus.PENDING) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Plant hire request must be in PENDING status to reject");
        }

        plantHireRequest.setStatus(RequestStatus.REJECTED);
        plantHireRequestRepository.save(plantHireRequest);

        return plantHireRequestAssembler.toModel(plantHireRequest);
    }

    public PlantHireRequestDTO cancel(Long id) throws Exception {
        PlantHireRequest plantHireRequest = plantHireRequestRepository.findById(id).orElse(null);

        if (plantHireRequest == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find plant hire request with id");
        }

        if (LocalDate.now().isAfter(plantHireRequest.getRentalPeriod().startDate())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not cancel plant hire request after the start date");
        }

        RequestStatus currentStatus = plantHireRequest.getStatus();
        if (currentStatus == RequestStatus.REJECTED || currentStatus == RequestStatus.CANCELLED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Plant hire request must be in PENDING or ACCEPTED status to cancel");
        }

        plantHireRequest.setStatus(RequestStatus.CANCELLED);
        plantHireRequestRepository.save(plantHireRequest);

        return plantHireRequestAssembler.toModel(plantHireRequest);
    }

    public PlantHireRequestDTO create(LocalDate start, LocalDate end, String plantReference, String siteId, String siteEngineerName, String supplier, RequestStatus status, BigDecimal price) throws Exception {
        if (start == null) {
            throw new Exception("start cannot be null");
        }

        if (end == null) {
            throw new Exception("end cannot be null");
        }

        //TODO: add validator
        /*DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            throw new Exception("Invalid start/end dates");
        }*/

        if (siteId == null) {
            throw new Exception("siteId cannot be null");
        }

        if (siteEngineerName == null || siteEngineerName.isEmpty()) {
            throw new Exception("siteEngineerName cannot be null or empty");
        }

        if (supplier == null) {
            throw new Exception("supplier cannot be null");
        }

        BusinessPeriod businessPeriod = BusinessPeriod.of(start, end);

        BigDecimal cost = price.multiply(BigDecimal.valueOf(DAYS.between(businessPeriod.getStartDate(), businessPeriod.getEndDate())));

        if (cost == null || BigDecimal.valueOf(0).compareTo(cost) == 1) {
            throw new Exception("cost cannot be null or less than 0");
        }
        PlantHireRequest plantHireRequest = PlantHireRequest.of(plantReference, businessPeriod, siteId, siteEngineerName, supplier, status, cost);
        PlantHireRequest savedRequest = plantHireRequestRepository.save(plantHireRequest);

        return plantHireRequestAssembler.toModel(savedRequest);
    }


    public PlantHireRequestDTO update(Long id, LocalDate start, LocalDate end, String plantReference, String siteId, String siteEngineerName, String supplier, RequestStatus status, BigDecimal price) throws Exception {
        PlantHireRequest plantHireRequest = plantHireRequestRepository.findById(id).orElse(null);

        if (plantHireRequest == null) {
            throw new Exception("Requested PlantHireRequest not found");
        }

        if (plantHireRequest.getStatus() != RequestStatus.PENDING) {
            throw new Exception("Status is not PENDING any more");
        }

        BusinessPeriod businessPeriod = BusinessPeriod.of(start, end);

        BigDecimal cost = price.multiply(BigDecimal.valueOf(DAYS.between(businessPeriod.getStartDate(), businessPeriod.getEndDate())));

        plantHireRequest.setPrice(cost);
        plantHireRequest.setRentalPeriod(businessPeriod);
        plantHireRequest.setSiteEngineerName(siteEngineerName);
        plantHireRequest.setSiteId(siteId);
        plantHireRequest.setStatus(RequestStatus.PENDING);
        plantHireRequest.setSupplier(supplier);

        PlantHireRequest savedRequest = plantHireRequestRepository.save(plantHireRequest);

        return plantHireRequestAssembler.toModel(savedRequest);
    }
}
