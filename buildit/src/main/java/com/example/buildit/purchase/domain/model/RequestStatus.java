package com.example.buildit.purchase.domain.model;

public enum RequestStatus {
    PENDING, APPROVED, REJECTED, CANCELLED
}
