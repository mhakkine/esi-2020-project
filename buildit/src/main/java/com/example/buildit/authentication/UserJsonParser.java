package com.example.buildit.authentication;

public class UserJsonParser {
    private String username;
    private String password;

    public UserJsonParser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
