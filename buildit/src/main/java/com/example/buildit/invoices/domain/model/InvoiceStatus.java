package com.example.buildit.invoices.domain.model;

public enum InvoiceStatus {
    PAID, UNPAID, APPROVED, REJECTED, REMINDED
}
