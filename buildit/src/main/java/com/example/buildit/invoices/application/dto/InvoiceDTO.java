package com.example.buildit.invoices.application.dto;

import com.example.buildit.invoices.domain.model.InvoiceStatus;
import com.example.buildit.purchase.application.dto.PlantHireRequestDTO;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class InvoiceDTO extends RepresentationModel<InvoiceDTO> {
    Long _id;
    PlantHireRequestDTO plantRequest;
    LocalDate dueDate;
    BigDecimal amount;
    InvoiceStatus status;
    String purchaseOrderReference;
    LocalDate paymentDate;
}
