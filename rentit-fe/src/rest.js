import axios from 'axios'
import auth from './auth'
import errorPopup from './components/popUp'

const baseUrl = 'http://localhost:8090/api/'

export function post (target, requestBody) {
  const headers = {
    headers: auth.getAuthHeader(),
    crossdomain: true
  }

  return axios.post(baseUrl + target, requestBody, headers)
    .then(response => {
      return response.data
    })
    .catch(error => {
      handleError(error)
    })
}

export function get (target, params = {}) {
  const queryConf = {
    headers: Object.assign({}, auth.getAuthHeader(), {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }),
    params: params,
    crossdomain: true
  }

  return axios.get(baseUrl + target, queryConf)
    .then(response => {
      return response.data
    })
    .catch(error => {
      handleError(error)
    })
}

function handleError (error) {
  const {status} = error.response
  if (status === 401) {
    errorPopup.showErrorPopup(error.response.data.error.message)
  } else if (status === 400) {
    errorPopup.showErrorPopup(error.response.data.error.message)
  } else {
    errorPopup.showErrorPopup(error.response.data.error.message)
  }
}
