import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import Login from '../components/Login'
import PlantHireRequest from '../components/PlantHireRequest'
import PlantHireRequests from '../components/PlantHireRequests'
import NewPlantHireRequest from '../components/NewPlantHireRequest'
import ModifyPlantHireRequest from '../components/ModifyPlantHireRequest'
import PurchaseOrders from '../components/PurchaseOrders'
import Invoices from '../components/Invoices'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/plant-hire-requests',
      name: 'PlantHireRequests',
      component: PlantHireRequests
    },
    {
      path: '/plant-hire-requests/:id',
      name: 'PlantHireRequests',
      component: PlantHireRequest
    },
    {
      path: '/new-plant-hire-request',
      name: 'NewPlantHireRequest',
      component: NewPlantHireRequest
    },
    {
      path: '/edit/:id',
      name: 'ModifyPlantHireRequest',
      component: ModifyPlantHireRequest
    },
    {
      path: '/purchaseOrders',
      name: 'PurchaseOrders',
      component: PurchaseOrders
    },
    {
      path: '/invoices',
      name: 'Invoices',
      component: Invoices
    }
  ]
})
