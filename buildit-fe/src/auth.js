import axios from 'axios'
import errorPopup from './components/popUp'

const baseUrl = 'http://localhost:8080/api'
var authTokenKey = 'Bearer'
var userNameKey = 'Username'

export default {
  user: {username: ''},
  login: function (context, creds, redirect) {
    const headers = {headers: {'Content-Type': 'application/json'}}
    const data = {'username': creds.username, 'password': creds.password}
    axios.post(baseUrl + '/authenticate', data, headers)
      .then(response => {
        this.user.username = creds.username
        var parts = response.headers.authorization.split(' ')
        authTokenKey = parts[0]
        window.localStorage.setItem(parts[0], parts[1])
        window.localStorage.setItem(userNameKey, creds.username)
        if (redirect) { context.$router.push({path: redirect}) }
        console.log('error')
      })
      .catch(error => {
        const {status} = error.response
        if (status === 401) {
          errorPopup.showErrorPopup('Authentication failed', 'Bad credentials')
        } else {
          errorPopup.showErrorPopup('Something went wrong', 'server failed')
        }
      })
  },

  logout: function (context) {
    if (this.authenticated()) {
      window.localStorage.removeItem(authTokenKey)
      window.localStorage.removeItem(userNameKey)
    }
  },

  isAdmin: function () {
    return window.localStorage.getItem(userNameKey) === 'works'
  },
  authenticated: function () {
    const jwt = window.localStorage.getItem(authTokenKey)
    return !!jwt
  },
  getAuthHeader: function () {
    return {
      authTokenKey: window.localStorage.getItem(authTokenKey)
    }
  }
}
