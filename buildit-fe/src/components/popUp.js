import Vue from 'vue'

export default {
  showErrorPopup: function (title, text) {
    this.showPopup(title, text, 'error')
  },
  showInfoPopup: function (title, text) {
    this.showPopup(title, text, 'info')
  },
  showSuccessPopup: function (title, text) {
    this.showPopup(title, text, 'success')
  },
  showPopup: function (title, text, type) {
    Vue.notify({
      group: 'buildit-messages',
      type: type,
      title: title,
      text: text
    })
  }
}
